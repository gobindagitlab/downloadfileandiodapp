package com.example.downloadfileandroidapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class MainActivity extends AppCompatActivity {
    private TextView progress_normal_tv,circular_progress_tv;
    private ProgressBar normal_progress,circular_progress;
    private  ImageView imageView;
    private RelativeLayout downloaslayout;

    int status = 0;
    private static final String url = "http://i.imgur.com/VpQW4oS.jpg";
    private static final String TAG = "MainActivity";

    @SuppressLint("SdCardPath")
    private static final String filePath = "/sdcard/MyFiles/downloadedfile.jpg";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progress_normal_tv = (TextView) findViewById(R.id.textView1);
        normal_progress = (ProgressBar) findViewById(R.id.progressBar1);
        circular_progress_tv = (TextView) findViewById(R.id.textView2);
        circular_progress = (ProgressBar) findViewById(R.id.progressBar2);
        imageView = (ImageView) findViewById(R.id.image);
        downloaslayout = (RelativeLayout) findViewById(R.id.layout);


        new DownloadFileFromURLTask().execute(url);

    }



    private class DownloadFileFromURLTask extends AsyncTask<String, String, String> {

        /**
         * Downloading file in background thread
         * */
        @SuppressLint("SdCardPath")
        @Override
        protected String doInBackground(String... f_url) {

            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream(filePath);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {

            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            normal_progress.setProgress(Integer.parseInt(progress[0]));
            circular_progress.setProgress(Integer.parseInt(progress[0]));
            progress_normal_tv.setText(progress[0]);
            circular_progress_tv.setText(progress[0]);
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        @SuppressLint("SdCardPath")
        @Override
        protected void onPostExecute(String file_url) {


            // dismiss progressbars after the file was downloaded
            downloaslayout.setVisibility(View.GONE);

            // Displaying downloaded image into image view
            // Reading image path from sdcard
            File imgFile = new File(filePath);

            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
                        .getAbsolutePath());
                imageView.setImageBitmap(myBitmap);
            }

            Toast.makeText(getApplicationContext(), "Download successful!",
                    Toast.LENGTH_SHORT).show();
        }


    }
}
